package pl.sii.mmitura

import com.mapr.db.spark.sql._
import com.mapr.db.spark.streaming.toDStreamFunctions
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.DataTypes
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import org.apache.spark.streaming.dstream.{DStream, InputDStream}
import org.apache.spark.streaming.kafka09.{ConsumerStrategies, KafkaUtils, LocationStrategies}
import org.apache.spark.streaming.{Duration, Seconds, StreamingContext}

import java.util.{Date, Properties, UUID}


object Job {

  val spark: SparkSession = SparkSession.builder()
    .appName("jobThree")
    .master("local[*]")
    .getOrCreate()

  val ssc = new StreamingContext(spark.sparkContext, Seconds(1))
  val generateUUID: UserDefinedFunction = udf(() => UUID.randomUUID().toString)

  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.ERROR)
    val countriesDf: DataFrame = spark.loadFromMapRDB("/datalake/data/country").cache()
    val stream = getReadStream
    val mappedStream: DStream[String] = stream.map(record => record.value())
    val enrichedMoviesStream = mappedStream.transform(rdd => transformMovieStreamData(rdd, countriesDf))
    enrichedMoviesStream.saveToMapRDB("/datalake/data/movie_enriched_with_country", createTable = false, bulkInsert = true, idFieldPath = "_id")
    stream.foreachRDD(rdd => sendMessageToStream(rdd))
    val genreStreamWindow = mappedStream.window(Duration(15000))
    val transformedGenreStreamWindow = genreStreamWindow.transform(rdd => getGenreWithUUID(rdd))
    transformedGenreStreamWindow.saveToMapRDB("/datalake/data/genre", createTable = false, bulkInsert = true, idFieldPath = "_id")
    ssc.start()
    ssc.awaitTermination()
  }

  private def transformMovieStreamData(rdd: RDD[String], countriesDf: DataFrame) = {
    if (!rdd.isEmpty()) {
      println(new Date() + ": messages received - Processing Stream Data")
      val ds = mapRddToMovieDataset(rdd)
      val enrichedDs = enrichMovieDataset(ds, countriesDf)
      enrichedDs.rdd
    }
    else {
      println(new Date() + ": No messages available for processing...")
      spark.emptyDataFrame.rdd
    }
  }

  def enrichMovieDataset(movies: Dataset[Movie], countriesDf: DataFrame): DataFrame = {
    val splitCountryIds = movies.withColumn("country_id",
      split(col("country_id"), ",").cast(DataTypes.createArrayType(DataTypes.StringType)))
      .withColumnRenamed("country_id", "country_ids")
      .select(col("_id"), explode(col("country_ids")).as("country_id"))
      .drop("country_ids")

    val countriesRenamedId = countriesDf.withColumnRenamed("_id", "country_id")
    val moviesWithCountries = splitCountryIds.join(countriesRenamedId, "country_id")
      .groupBy("_id")
      .agg(concat_ws(",", collect_list("country")).alias("country"))
      .drop("country_id")

    movies.join(moviesWithCountries, "_id")
      .withColumnRenamed("_id", "imdb_title_id")
      .withColumn("_id", generateUUID()).cache()
  }

  private def sendMessageToStream(rdd: RDD[ConsumerRecord[String, String]]): Unit = {
    rdd.foreachPartition(partition => {
      val producer = new KafkaProducer[String, String](getProps)
      partition.foreach(record => {
        val message = new ProducerRecord[String, String]("/datalake/stream/movie:index", record.key(), "")
        producer.send(message)
      })
    })
  }

  def getExplodedGenre(movieDf: Dataset[Movie]): DataFrame = {
    movieDf.select(explode(split(col("genre"), ",")).as("genre"))
      .select(trim(col("genre")).as("genre"))
      .distinct()
      .withColumn("_id",generateUUID())
  }

  private def mapRddToMovieDataset(rdd: RDD[String]) = {
    import spark.implicits._
    val jsonSchema = spark.read.json(rdd).schema
    spark.read.schema(jsonSchema).json(rdd).as[Movie]
  }

  private def getGenreWithUUID(rdd: RDD[String]) = {
    if (!rdd.isEmpty()) {
      getExplodedGenre(mapRddToMovieDataset(rdd)).rdd
    }
    else {
      spark.emptyDataFrame.rdd
    }
  }

  def getReadStream: InputDStream[ConsumerRecord[String, String]] = {
    val streamParams: Map[String, Object] = Map(
      "key.serializer" -> classOf[StringSerializer],
      "value.serializer" -> classOf[StringSerializer],
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "auto.offset.reset" -> "latest",
      "enable.auto.commit" -> false.asInstanceOf[Object]
    )
    val topics = Array("/datalake/stream/movie:read")
    KafkaUtils
      .createDirectStream(ssc, LocationStrategies.PreferConsistent, ConsumerStrategies.Subscribe[String, String](topics, streamParams + ("group.id" -> "job_three")))
  }

  def getProps: Properties = {
    val p = new Properties()
    p.setProperty("key.serializer", classOf[StringSerializer].getName)
    p.setProperty("value.serializer", classOf[StringSerializer].getName)
    p
  }

  case class Movie(_id: String, title: String,
                   original_title: String, year: Long,
                   date_published: String, genre: String,
                   duration: String, language: String,
                   director: String, writer: String,
                   production_company: String, actors: String,
                   description: String, avg_vote: Double,
                   votes: Long, budget: String,
                   usa_gross_income: String, worldwide_gross_income: String,
                   metascore: String, reviews_from_users: Double,
                   reviews_from_critics: Double, country_id: String)

}
